package hr.apps.mosaic.example_92;

/**
 * Created by Stray on 19.9.2017..
 */

public class Player {
	private String mUid;
	private String mName;
	private int mPoints;
	private int mGold;

	public Player() {
	}

	public Player(String uid, String name, int points, int gold) {
		mUid = uid;
		mName = name;
		mPoints = points;
		mGold = gold;
	}

	public String getUid() {
		return mUid;
	}

	public void setUid(String uid) {
		mUid = uid;
	}

	public String getName() {
		return mName;
	}

	public void setName(String name) {
		mName = name;
	}

	public int getPoints() {
		return mPoints;
	}

	public void setPoints(int points) {
		mPoints = points;
	}

	public int getGold() {
		return mGold;
	}

	public void setGold(int gold) {
		mGold = gold;
	}

	@Override
	public String toString() {
		return "mName='" + mName + '\'' +
				", mPoints=" + mPoints +
				", mGold=" + mGold;
	}
}
