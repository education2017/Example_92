package hr.apps.mosaic.example_92;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends Activity implements ValueEventListener {

	private FirebaseDatabase mFirebaseDatabase;
	private Random mGenerator = new Random();
	private ArrayAdapter<String> mArrayAdapter;



	@BindView(R.id.bSavePlayer)	Button bSavePlayer;
	@BindView(R.id.etName) EditText etName;
	@BindView(R.id.lvPlayers) ListView lvPlayers;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		ButterKnife.bind(this);
		this.mArrayAdapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_expandable_list_item_1);
		this.lvPlayers.setAdapter(this.mArrayAdapter);
		this.mFirebaseDatabase = FirebaseDatabase.getInstance();
		this.mFirebaseDatabase.setPersistenceEnabled(true);
		this.mFirebaseDatabase
				.getReference()
				.child("players")
				.addValueEventListener(this);
	}

	@OnClick(R.id.bSavePlayer)
	public void savePlayerClicked(){

		String name = this.etName.getText().toString();
		int points = this.mGenerator.nextInt(1000);
		int gold = this.mGenerator.nextInt(1000);

		DatabaseReference playerReference = this.mFirebaseDatabase.getReference("players");
		String key = playerReference.push().getKey();
		Player player = new Player(key, name, points, gold);
		playerReference.child(key).setValue(player);
	}

	@Override
	public void onDataChange(DataSnapshot dataSnapshot) {
		List<String> players = new ArrayList<>();
		for(DataSnapshot playerSnapshot : dataSnapshot.getChildren()){
			Player player = playerSnapshot.getValue(Player.class);
			players.add(player.toString());
		}
		mArrayAdapter.clear();
		mArrayAdapter.addAll(players);
		mArrayAdapter.notifyDataSetChanged();
	}

	@Override
	public void onCancelled(DatabaseError databaseError) {
		Log.d("TAG", "Cancel");
	}
}
